<?php
declare(strict_types=1);

namespace App\Config;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

class WebhooksConfiguration implements ConfigurationInterface
{
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('webhooks');

        $rootNode
            ->useAttributeAsKey('name')
            ->cannotBeEmpty()
            ->arrayPrototype()
            ->children()
                ->scalarNode('condition')->end()
                ->scalarNode('exec')->end()
            ->end()
        ->end();

        return $treeBuilder;
    }
}
