<?php
declare(strict_types=1);

namespace App;

use App\Config\WebhooksConfiguration;
use App\Events\EventInterface;
use Symfony\Component\Config\Definition\Processor;
use Symfony\Component\ExpressionLanguage\ExpressionLanguage;
use Symfony\Component\Yaml\Yaml;

class Webhooks
{
    private $configs;

    public function __construct(string $configFile)
    {
        $processor = new Processor();
        $this->configs = $processor->processConfiguration(
            new WebhooksConfiguration(),
            Yaml::parseFile($configFile)
        );
    }

    public function matching(EventInterface $event): array
    {
        $expression = new ExpressionLanguage();

        return array_filter($this->configs, function ($config) use ($expression, $event) {
            return $expression->evaluate($config['condition'], ['event' => $event]);
        });
    }
}
