<?php
declare(strict_types=1);

namespace App\Controller;

use App\EventFactory;
use App\Webhooks;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route as Route;

class DefaultController extends Controller
{
    /**
     * @Route("/")
     */
    public function indexAction(EventFactory $eventFactory, Webhooks $webhooks, LoggerInterface $logger)
    {
        $event = $eventFactory->getEvent();

        if ($event) {
            $matches = $webhooks->matching($event);

            foreach ($matches as $name => $hook) {
                $logger->info("Running webhook “{$name}”...");

                exec($hook['exec'], $output, $exit);

                if ($exit === 0) {
                    $logger->info("Webhook {$name} ran.", [
                        'output' => $output,
                    ]);
                } else {
                    $logger->warning("Webhook {$name} failed.", [
                        'output' => $output,
                        'exit_status' => $exit,
                    ]);
                }
            }

            return $this->json([
                'success' => true,
                'message' => "Ok.",
                'webhooks_invoked' => array_keys($matches),
            ]);
        } else {
            return $this->json([
                'success' => false,
                'message' => "Couldn't parse event.",
            ]);
        }

    }
}
