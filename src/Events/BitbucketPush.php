<?php
declare(strict_types=1);

namespace App\Events;

use Symfony\Component\HttpFoundation\Request;

final class BitbucketPush implements EventInterface
{
    /**
     * @var array<string>
     */
    private $branches;

    /**
     * @var string
     */
    private $repository;

    static public function fromRequest(Request $request): ?EventInterface
    {
        if (!self::supports($request)) return null;

        $event = new self();

        // parse branches
        $event->branches = [];
        $changes = $request->request->get('push')['changes'];
        foreach ($changes as $change) {
            if ($change['new']['type'] === 'branch') {
                $event->branches[] = $change['new']['name'];
            }
        }

        $event->repository = $request->request->get('repository')['full_name'];

        return $event;
    }

    static public function supports(Request $request): bool
    {
        return all([
            $request->headers->get('Content-Type') === 'application/json',
            $request->headers->get('User-Agent') === 'Bitbucket-Webhooks/2.0',
            $request->headers->get('X-Event-Key') === 'repo:push',
        ]);
    }

    public function getType(): string
    {
        return 'push';
    }

    public function getBranches(): array
    {
        return $this->branches;
    }

    public function getRepository(): string
    {
        return $this->repository;
    }
}
