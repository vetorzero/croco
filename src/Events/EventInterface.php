<?php
declare(strict_types=1);

namespace App\Events;

use Symfony\Component\HttpFoundation\Request;

interface EventInterface
{
    public function getType(): string;

    /**
     * @return array<string>
     */
    public function getBranches(): array;

    public function getRepository(): string;

    static public function fromRequest(Request $request): ?EventInterface;

    static public function supports(Request $request): bool;
}
