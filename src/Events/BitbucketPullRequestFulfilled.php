<?php
declare(strict_types=1);

namespace App\Events;

use Symfony\Component\HttpFoundation\Request;

final class BitbucketPullRequestFulfilled implements EventInterface
{
    /**
     * @var string
     */
    private $branch;

    /**
     * @var string
     */
    private $repository;

    static public function fromRequest(Request $request): ?EventInterface
    {
        if (!self::supports($request)) return null;

        $event = new self();

        $event->branch = $request->request->get('pullrequest')['destination']['branch']['name'];
        $event->repository = $request->request->get('pullrequest')['destination']['repository']['full_name'];

        return $event;
    }

    static public function supports(Request $request): bool
    {
        return all([
            $request->headers->get('Content-Type') === 'application/json',
            $request->headers->get('User-Agent') === 'Bitbucket-Webhooks/2.0',
            $request->headers->get('X-Event-Key') === 'pullrequest:fulfilled',
        ]);
    }

    public function getType(): string
    {
        return 'pull-request-fulfilled';
    }

    public function getBranches(): array
    {
        return [$this->branch];
    }

    public function getRepository(): string
    {
        return $this->repository;
    }
}
