<?php
declare(strict_types=1);

namespace App;

use App\Events\EventInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;

class EventFactory
{
    /**
     * @var array|string[]
     */
    private $eventClasses;

    /**
     * @var null|Request
     */
    private $request;

    /**
     * @param string[] $eventClasses
     * @param RequestStack $requestStack
     */
    public function __construct(array $eventClasses, RequestStack $requestStack)
    {
        $this->eventClasses = $eventClasses;
        $this->request = $requestStack->getCurrentRequest();
    }

    /**
     * Tries to find an event matching the request.
     * @return EventInterface|null
     */
    public function getEvent(): ?EventInterface
    {
        if ($this->request) {
            foreach ($this->eventClasses as $eventClass) {
                $event = $eventClass::fromRequest($this->request);

                if ($event) {
                    return $event;
                }
            }
        }

        return null;
    }
}
