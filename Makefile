deps :
	composer install

cache-clear :
	bin/console cache:clear --no-warmup

cache-warmup :
	bin/console cache:warmup

update : deps cache-clear cache-warmup
