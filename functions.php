<?php

/**
 * Dump stuff and then die.
 *
 * @param array ...$stuff
 */
function dd(...$stuff)
{
    dump(...$stuff);
    die;
}


function every(callable $f, iterable $xs): bool
{
    foreach ($xs as $x) {
        if (!$f($x)) {
            return false;
        }
    }

    return true;
}

function some(callable $f, iterable $xs): bool
{
    foreach ($xs as $x) {
        if ($f($x)) {
            return true;
        }
    }

    return false;
}

function is_truthy($x): bool
{
    return !!$x;
}

function is_falsy($x): bool
{
    return !$x;
}

function all(iterable $xs): bool
{
    return every('is_truthy', $xs);
}
